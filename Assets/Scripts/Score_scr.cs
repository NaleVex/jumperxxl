﻿using UnityEngine;
using UnityEngine.UI;

public class Score_scr : MonoBehaviour {

	public float playerSpeed = 1f;
	public Text scoreText;

	private int score = 0;
	private float tempScore = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		tempScore += playerSpeed * 0.02f;
		score = (int)tempScore;
		//Debug.Log(tempScore);
		playerSpeed = score / 10 + 1;
		scoreText.text = "Score: " + score;
	}

}
