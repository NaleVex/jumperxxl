﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	private static int enemyCount = 0;
	public float MoveSpeed;
	public GameObject gameManager;
	private Rigidbody2D rb;
	private Score_scr score;
	
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		score = gameManager.GetComponent<Score_scr>();
		MoveSpeed *= score.playerSpeed;		
		enemyCount++;
		Debug.Log("screen width " + Screen.width.ToString());
		Debug.Log("Enemy count: " + enemyCount.ToString());
	}
	
	// Update is called once per frame
	void Update () {

		//Vector2 leftForce = new Vector2(this.transform.position.x + MoveSpeed,0);
		//rb.velocity -= new Vector2(MoveSpeed,0);
		rb.position = new Vector2(this.transform.position.x - MoveSpeed,this.transform.position.y);
		Debug.Log("Speed: " + rb.position.magnitude.ToString());
	}


}
